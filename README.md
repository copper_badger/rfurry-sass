# rfurry-sass
SASS Source for the Furry Subreddit Theme

## Development Setup

This project, not surprisingly, is compiled with SASS. Feel free to use your
own tools (the main file is `src/main.scss`). If you don't have anything to
compile SASS with, then follow these steps and you will!

First, install `npm` via [NodeJS](https://nodejs.org).

Next, use `npm` to globally install `gulp`, a job automation tool:

    npm install -g gulp

Then, install the necessary build dependencies from within the project's
directory:

    npm install

When you're ready to build the stylesheet, run the `build` task from within
the project's directory:

    gulp build

This will create `dist/furry.css`. Run the `minify` task to creat a minified
`furry.min.css`:

    gulp minify
